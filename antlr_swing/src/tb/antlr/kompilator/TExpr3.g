tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
  Integer loopnumber = 0;
}
prog    : (e+=codeblock | e+=expr | d+=decl)* -> template(name={$e},deklaracje={$d}) "<deklaracje> start: <name;separator=\" \n\"> ";

codeblock : ^(BEGIN   (e+=codeblock | e+=expr | d+=decl)* ) -> block(wyr={$e}, deklaracje={$d}) ;
decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> dodaj(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> odejmij(p1={$e1.st},p2={$e2.st})
        | ^(COMPARE e1=expr e2=expr) -> compare(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> pomnoz(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> podziel(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) {globals.hasSymbol($i1.text);} -> podst(id={$ID},p1={$e2.st})
        | ^(WHILE e1=expr b2=codeblock) {loopnumber++;} -> whileloop(condition={$e1.st},code={$b2.st},loop_no={loopnumber.toString()})
        | i1=ID  {globals.hasSymbol($i1.text);} -> wczyt(id={$i1.text})
        | INT  {numer++;}                    -> int(i={$INT.text},j={numer.toString()})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

